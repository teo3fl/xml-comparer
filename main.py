from xml_comparer import XmlComparer

objects = ['Mesh', 'Mesh2', 'NXOpen.CAE.Connections.Bolt', 'random object']
outputs = ['section_orientation', 'section_offset', 'rigid y translation', 'Number of elements:', 'random output']

XmlComparer.compare('original1.xml', 'other1.xml', objects, outputs, 'log_file.txt')
