import xml.etree.ElementTree as ET
from texttable import Texttable
import numpy as np


# The 'XmlComparer' class contains methods that aid in comparing 2 given xml trees. It contains 3 members:
#   'original': holds data from the first xml (see 'XmlContainer')
#   'other': holds data from the second xml (see 'XmlContainer')
#   'log_path': string that holds the path of the output file
#
# Examples:
#
# XmlComparer.compare(xml_1_path, xml_2_path, array_of_objects_as_strings, array_of_outputs_as_strings)
# XmlComparer.compare(xml_1_path, xml_2_path, array_of_objects_as_strings, array_of_outputs_as_strings, log_file_path)

class XmlComparer(object):
    __original = None
    __other = None
    __log_path = None

    @staticmethod
    def compare(xml_1, xml_2, objects, outputs, log_file):
        XmlComparer.__init()
        XmlComparer.__log_path = log_file

        XmlComparer.__log(
            '********************************************************************************\n' +
            '                                  XML Comparer                                  \n' +
            '********************************************************************************\n\n')

        # read files
        if not (XmlComparer.__read_files(xml_1, xml_2)):
            return

        XmlComparer.__log(f'Original file: {xml_1}\n')
        XmlComparer.__log(f'Other file: {xml_2}\n\n')

        # extract the relevant objects from both xml files and fill the ('original' and 'other') XmlContainers with data
        XmlComparer.__extract_data(objects, outputs)

        # check if both files have the same structure
        XmlComparer.__compare_structures()

        # check if all given objects were found inside the files
        XmlComparer.__check_unused_objects(objects)

        # check is all given types of output were found
        XmlComparer.__check_unused_outputs(outputs)

        # finally, look for value differences and missing elements
        XmlComparer.__search_differences()

    @staticmethod
    def __init():
        XmlComparer.__original = _XmlContainer()
        XmlComparer.__other = _XmlContainer()

    @staticmethod
    def __log(message):
        file = open(XmlComparer.__log_path, "a")
        file.write(message)
        file.close()

    @staticmethod
    def __read_files( xml_1, xml_2):
        return XmlComparer.__read_file(XmlComparer.__original, xml_1) and XmlComparer.__read_file(XmlComparer.__other, xml_2)

    @staticmethod
    def __read_file(xml_container_, file_path):
        try:
            xml_container_.read_file(file_path)
        except FileNotFoundError:
            XmlComparer.__log(f'Could not find the file at {file_path}.')
            return False
        except ET.ParseError:
            XmlComparer.__log(f'Could not parse the file at {file_path}.')
            return False

        return True

    @staticmethod
    def __extract_data(object_types, output_names):
        XmlComparer.__original.extract_objects(object_types, output_names)
        XmlComparer.__other.extract_objects(object_types, output_names)

    @staticmethod
    def __compare_structures():
        if _XmlContainer.have_same_structure(XmlComparer.__original.tree, XmlComparer.__other.tree):
            XmlComparer.__log('Both files have the same tree structure.\n\n\n')
        else:
            XmlComparer.__log('Warning: the XML trees do not have the same structure.\n\n\n')

    @staticmethod
    def __check_unused_objects( object_types):
        unused_types = XmlComparer.__original.get_unused_object_types(object_types)

        if len(unused_types) == 0:
            XmlComparer.__log('Found all object types inside the original file.\n\n')
        else:
            XmlComparer.__log('Warning: the following object types were not found in the original xml:\n')
            XmlComparer.__log('\n'.join(map(str, unused_types)) + '\n\n')

        unused_types = XmlComparer.__other.get_unused_object_types(object_types)

        if len(unused_types) == 0:
            XmlComparer.__log('Found all object types inside the other file.\n\n\n')
        else:
            XmlComparer.__log('Warning: the following object types were not found in the other xml:\n')
            XmlComparer.__log('\n'.join(map(str, unused_types)) + '\n\n\n')

    @staticmethod
    def __check_unused_outputs(output_names):
        unused_names = XmlComparer.__original.get_unused_output_names(output_names)

        if len(unused_names) != 0:
            XmlComparer.__log('Warning: the following output names were not found in the original xml:\n')
            XmlComparer.__log('\n'.join(map(str, unused_names)) + '\n\n')
        else:
            XmlComparer.__log('Found all output names inside the original file.\n\n')

        unused_names = XmlComparer.__other.get_unused_output_names(output_names)

        if len(unused_names) != 0:
            XmlComparer.__log('Warning: the following output names were not found in the other xml:\n')
            XmlComparer.__log('\n'.join(map(str, unused_names)) + '\n\n\n')
        else:
            XmlComparer.__log('Found all output names inside the other file.\n\n\n')

    @staticmethod
    def __search_differences():
        differences_table = Texttable()
        differences_table.add_rows([['', 'Object Type', 'Object Value', 'Output Name']])

        element_not_found_table = Texttable()
        element_not_found_table.add_rows([['', 'Object Type', 'Object Value', 'Missing in the other xml']])

        missing_elements_count = 0
        mismatches_count = 0

        # The following for loop will iterate through the output nodes found inside the original xml (the output
        # nodes are contained inside 'ObjectNodes' objects, which are contained inside the 'original.objects' list),
        # and will look for differences in values and for missing equivalent nodes.
        #
        # For each output node, it will search for a corresponding output node in the other xml (the 'other_node'
        # must have the same 'output_name', and its parent must have the same 'object_type' and 'object_value' as the
        # output node from the original xml. If a node is found and the two nodes do not have the same values,
        # the node from the original xml will be added to the 'differences_table'. If a node is not found in the
        # other xml, the current node is added to the 'element_not_found_table'.

        for object_ in XmlComparer.__original.objects:
            for node in object_.outputs:
                object_type = object_.node.attrib['type']
                object_value = object_.node.attrib['value']
                output_name = node.attrib['name']

                try:
                    other_node = XmlComparer.__other.get_output_node(object_value, output_name)
                    if not (_XmlContainer.have_same_values(node, other_node)):
                        mismatches_count += 1
                        differences_table.add_row([mismatches_count, object_type, object_value, output_name])

                except ValueError:
                    missing_elements_count += 1
                    element_not_found_table.add_row(
                        [missing_elements_count, object_type, object_value, 'object with the same value'])
                except NameError:
                    missing_elements_count += 1
                    element_not_found_table.add_row(
                        [missing_elements_count, object_type, object_value,
                         'output with the name "' + output_name + '\"'])

        if missing_elements_count > 0:
            XmlComparer.__log('The following elements are missing from the other xml:\n')
            XmlComparer.__log(element_not_found_table.draw() + '\n\n\n')

        if mismatches_count > 0:
            XmlComparer.__log('The following mismatched values were found:\n')
            XmlComparer.__log(differences_table.draw() + '\n\n\n')
        else:
            XmlComparer.__log('No value mismatches found.\n\n\n')


# Auxiliary classes for the XmlComparer


# The 'XmlContainer' class manages the data from an xml file, in terms of reading and parsing the xml tree, and
# searching for relevant nodes. It contains 2 members:
#   'tree': holds the parsed tree which is read from a given xml file
#   'objects': list that contains the relevant object nodes extracted from the tree (see 'ObjectNode')
class _XmlContainer:

    def __init__(self):
        self.tree = None
        self.objects = []

    def read_file(self, file_path):
        # This method will parse a tree from a given xml file into the 'tree' variable if the file can be found,
        # otherwise it will raise an error if the given file is missing or cannot be parsed.
        self.tree = ET.parse(file_path)

    def extract_objects(self, object_types, output_names):
        # This method will extract the object nodes from the xml if the 'type' attribute of the node is found inside
        # the 'object_types' list AND if this node has at least one child node where the 'name' attribute is found
        # inside the 'output_names', and will insert it into the 'self.objects' list.
        #
        # When a node with a matching 'type' value is found, it is used to create an 'ObjectNode'. Then the relevant
        # children of the node will be inserted in the 'ObjectNode.outputs' list (see the
        # 'ObjectNode.extract_outputs' method for details). Finally, the 'ObjectNode' is inserted in the
        # 'self.object' list.

        for node in self.tree.findall('.//object'):
            if node.attrib['type'] in object_types:
                object_ = _ObjectNode(node)
                object_.extract_outputs(output_names)

                self.objects.append(object_)

    def get_unused_object_types(self, expected_types):
        # This method returns a list of strings that contains the types of objects that either were not found in the
        # given xml file or do not contain any relevant output nodes.
        #
        # It will iterate through the 'self.objects' list (which contains 'ObjectNode' objects), and will insert the
        # type of each node in the 'found_types' list IF the list does not already contain it. After iterating
        # through the nodes, it will return the difference between the content of the 'expected_types' list and the
        # 'found_types' list (the strings that do not appear in the 'found_types' list, but do appear in the
        # 'expected_types' list).

        found_types = []

        for object_ in self.objects:
            if not (object_.node.attrib['type'] in found_types):
                found_types.append(object_.node.attrib['type'])

        return np.setdiff1d(expected_types, found_types)

    def get_unused_output_names(self, expected_names):
        # This method returns a list of strings that contains the names of outputs that were not found in the
        # given xml file.
        #
        # It will iterate through the 'self.objects' list (which contains 'ObjectNode' objects), and for each
        # ObjectNode, it will iterate through its list of 'objects'. It will insert the name of each output node in
        # the 'found_names' list IF the list does not already contain it. After iterating through the nodes,
        # it will return the difference between the content of the 'expected_names' list and the 'found_names' list
        # (the strings that do not appear in the 'found_names' list, but do appear in the 'expected_names' list).

        found_names = []

        for object_ in self.objects:
            for node in object_.outputs:
                if not (node.attrib['name'] in found_names):
                    found_names.append(node.attrib['name'])

        return np.setdiff1d(expected_names, found_names)

    def get_output_node(self, object_value, output_name):
        # This method will return an output node where its name is 'output_name' and the value of its parent is
        # 'object_value'. If the given output is not found, it raises a 'NameError'. If no object of the given value
        # is found, it raises a 'ValueError'.
        #
        # It will iterate through the 'self.objects' list (which contains 'ObjectNode' objects) and search for nodes
        # where the 'value' attribute matches the given 'object_value'. If the value matches, then the tree contains
        # a node with the given value ('found_object' equals true, which might be used later for raising an error).
        # If the node contains an output with the given 'output_name', it is returned. If no matching object or
        # output is found, it will raise an error.

        found_object = False

        for object_ in self.objects:
            if object_.node.attrib['value'] == object_value:
                found_object = True

                if object_.contains_output(output_name):
                    return object_.get_node(output_name)

        if found_object:
            raise NameError
        else:
            raise ValueError

    @staticmethod
    def have_same_structure(xml_1, xml_2):
        # This method compares the structure of 2 xml trees. Returns true if they have the same structure of nodes,
        # otherwise returns false.
        xml_1_flattened = [i.tag for i in xml_1.iter()]
        xml_2_flattened = [i.tag for i in xml_2.iter()]

        return xml_1_flattened == xml_2_flattened

    @staticmethod
    def have_same_values(node_1, node_2):
        # This method compares the structure of 2 given nodes, including the values. Returns true if the nodes have the
        # same structure and values, otherwise returns false.
        return ET.tostring(node_1) == ET.tostring(node_2)


# The 'ObjectNode' class manages an xml node and its relevant children. It contains 2 members:
#   'node': the parent node (object node)
#   'outputs': the children of the parent node (output nodes)
class _ObjectNode:

    def __init__(self, node):
        self.node = node
        self.outputs = []

    def extract_outputs(self, output_types):
        # This method will iterate through the output children nodes of the parent 'self.node', and insert them in the
        # 'self.outputs' list if the 'name' attribute of the child is found inside the 'output_types' list.

        for sub_element in self.node.findall('.//output'):
            if sub_element.attrib['name'] in output_types:
                self.outputs.append(sub_element)

    def contains_output(self, output_name):
        # This method will iterate through the 'self.outputs' list and search for an output node where the 'name'
        # attribute of the node matches the given 'output_name'. If such node is found, it returns true, else it returns
        # false.

        for output in self.outputs:
            if output.attrib['name'] == output_name:
                return True

        return False

    def get_node(self, output_name):
        # This method will iterate through the 'self.outputs' list and search for an output node where the 'name'
        # attribute of the node matches the given 'output_name'. If such node is found, it returns the node, else it
        # raises a 'NameError'.

        for output in self.outputs:
            if output.attrib['name'] == output_name:
                return output

        raise NameError
